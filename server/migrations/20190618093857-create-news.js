'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('news', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    name: { type: Sequelize.STRING },
    text: { type: Sequelize.STRING },
    authorId: { type: Sequelize.INTEGER },
    tags: { type: Sequelize.STRING },
    img: { type: Sequelize.STRING },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  }),
  down: queryInterface => queryInterface.dropTable('news'),
};
