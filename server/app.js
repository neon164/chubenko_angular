const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const passport = require('passport');
const app = express();
const cors = require('cors');

const indexRouter = require('./routes/index');
const newsRouter = require('./routes/news');
const usersRouter = require('./routes/users');
const loginRouter = require('./routes/login');

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api/images', express.static(path.join(__dirname, 'public/images')));
app.use('/api/avatars', express.static(path.join(__dirname, 'public/avatars')));

app.use('/api/', indexRouter);
app.use('/api/news', newsRouter);
app.use('/api/userpage', usersRouter);
app.use('/api/login', loginRouter);

module.exports = app;
