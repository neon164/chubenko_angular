const express = require('express');
const router = new express.Router();
const userControllers = require('../controllers/users');

router.post('/', userControllers.login);

module.exports = router;
