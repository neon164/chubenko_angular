const express = require('express');
const router = new express.Router();
const newsControllers = require('../controllers/news');
const multer = require('multer');
const path = require('path');
const passport = require('passport');
const passportJWT = require('passport-jwt');
const { ExtractJwt } = passportJWT;
const JwtStrategy = passportJWT.Strategy;
const { user } = require('../models');
const secret = require('../JWToptions');

const jwtOptions = {};

jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = secret;

passport.use(new JwtStrategy(jwtOptions, (jwtPayload, next) => {
  const userDB = user.findOne({ where: { id: jwtPayload.id } });

  if (userDB) {
    next(null, userDB);
  } else {
    next(null, false, { message: 'Incorrect token.' });
  }
}));

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, '../public/images');
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const upload = multer({
  storage,
  fileFilter: (req, file, callback) => {
    const ext = path.extname(file.originalname);

    if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.JPG') {
      return callback(new Error('Only images are allowed'));
    }
    callback(null, true);
  },
});

router.use(passport.initialize());

router.get('/', newsControllers.getNews);
router.get('/:id', newsControllers.getByAuthor);
router.post('/', passport.authenticate('jwt', { session: false }), upload.single('img'), newsControllers.addNews);
module.exports = router;
