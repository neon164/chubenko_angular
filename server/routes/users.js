const express = require('express');
const router = new express.Router();
const HttpStatus = require('http-status-codes');
const userControllers = require('../controllers/users');
const multer = require('multer');
const path = require('path');
const passport = require('passport');
const passportJWT = require('passport-jwt');
const { ExtractJwt } = passportJWT;
const JwtStrategy = passportJWT.Strategy;
const { user } = require('../models');
const secret = require('../JWToptions');

const jwtOptions = {};

jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = secret;

passport.use(new JwtStrategy(jwtOptions, (jwtPayload, next) => {
  const userDB = user.findOne({ where: { id: jwtPayload.id } });

  if (userDB) {
    next(null, userDB);
  } else {
    next(null, false, { message: 'Incorrect token.' });
  }
}));

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, '../public/avatars');
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const upload = multer({
  storage,
  fileFilter: (req, file, callback) => {
    const ext = path.extname(file.originalname);

    if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.JPG') {
      return callback(new Error('Only images are allowed'));
    }
    callback(null, true);
  },
});

router.get('/:id', userControllers.getUser);
router.get('/', (req, res) => {
  res.status(HttpStatus.INTERNAL_SERVER_ERROR).send('ID required');
});
router.post('/social', userControllers.socialLogin);
router.post('/', userControllers.addUser);
router.put('/:id', passport.authenticate('jwt', { session: false }), upload.single('img'), userControllers.updateUser);

module.exports = router;
