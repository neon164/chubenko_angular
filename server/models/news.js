'use strict';

module.exports = (sequelize, DataTypes) => {
  const news = sequelize.define('news', {
    authorId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    text: DataTypes.TEXT,
    tags: DataTypes.STRING,
    img: DataTypes.STRING,
  }, {});

  news.associate = function(models) {
    news.belongsTo(models.user, {
      foreignKey: 'authorId',
      as: 'user',
    });
  };

  return news;
};
