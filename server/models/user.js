'use strict';

module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('user', {
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    img: DataTypes.STRING,
  }, {});

  users.associate = function(models) {
    users.hasOne(models.news, {
      foreignKey: 'authorId',
      as: 'user',
    });
  };

  return users;
};
