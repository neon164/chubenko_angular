const passportJWT = require('passport-jwt');
const secret = require('./JWToptions');
const { ExtractJwt } = passportJWT;
const JwtStrategy = passportJWT.Strategy;
const { user } = require('./models/user');

const jwtOptions = {};

jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = secret;

const strategy = new JwtStrategy(jwtOptions, (jwtPayload, next) => {
  const userDB = user.findOne({ where: { id: jwtPayload.id } });

  if (userDB) {
    next(null, userDB);
  } else {
    next(null, false);
  }
});

module.exports = strategy;
