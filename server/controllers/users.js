const { user } = require('../models/index');
const HttpStatus = require('http-status-codes');
const jwt = require('jsonwebtoken');
const passportJWT = require('passport-jwt');
const jwtOptions = {};
const secret = require('../JWToptions');
const { ExtractJwt } = passportJWT;

jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = secret;

module.exports = {
  async addUser(req, res) {
    try {
      const { body: { email, password, firstName, lastName, createdAt, updatedAt } } = req;

      await user.findOne({ where: { email: req.body.email } })
        .then(userDB => {
          if (userDB) {
            res.send('You\'re already registered! Try Log In');
          } else {
            const urlImg = 'api/avatars/default.png';

            user.create({
              email,
              password,
              firstName,
              lastName,
              createdAt,
              updatedAt,
              img: urlImg,
            })
              .then(userDB => {
                const payload = { id: userDB.id };
                const token = jwt.sign(payload, jwtOptions.secretOrKey);

                res.json({ id: userDB.id, token });
              });
          }
        });
    } catch (error) {
      res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  },
  async socialLogin(req, res) {
    try {
      const { body: { email, firstName } } = req;

      const userDB = await user.findOne({ where: { email: req.body.email } })
        .then(userDB => {
          if (userDB) {
            const payload = { id: userDB.id };
            const token = jwt.sign(payload, jwtOptions.secretOrKey);

            res.json({ id: userDB.id, token });
          } else {
            const urlImg = 'api/avatars/default.png';

            user.create({
              email,
              firstName,
              lastName: '',
              img: urlImg,
            })
              .then(userDB => {
                const payload = { id: userDB.id };
                const token = jwt.sign(payload, jwtOptions.secretOrKey);

                res.json({ id: userDB.id, token });
              });
          }
        });
    } catch (error) {
      res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  },
  async getUser(req, res) {
    try {
      const userDB = await user.findAll({
        where: { id: req.params.id },
        attributes: [
          'id',
          'email',
          'firstName',
          'lastName',
          'img',
        ],
      });

      res.send(userDB);
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .send('Error find user');
    }
  },
  async updateUser(req, res) {
    try {
      const body = JSON.parse(req.body.text);
      const { firstName, lastName } = body;

      if (req.file !== undefined) {
        const urlImg = 'api/avatars/' + req.file.filename;

        const userDB = await user.update({
          firstName,
          lastName,
          img: urlImg,
        }, { where: { id: req.params.id } });

        res.send(userDB);
      } else {
        const userDB = await user.update({
          firstName,
          lastName,
        }, { where: { id: req.params.id } });

        res.send(userDB);
      }
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .send('Error update user');
    }
  },
  async login(req, res) {
    try {
      await user.findOne({ where: { email: req.body.email } })
        .then(userDB => {
          if (!userDB) {
            res.status(HttpStatus.NOT_FOUND)
              .json({ message: 'no such user found' });
          } else if (userDB.password === req.body.password) {
            const payload = {
              email: userDB.email,
              id: userDB.id,
            };
            const token = jwt.sign(payload, jwtOptions.secretOrKey);

            res.json({
              user: {
                id: userDB.id,
                email: userDB.email,
                firstName: userDB.firstName,
                lastName: userDB.lastName,
                img: userDB.img,
              },
              token,
            });
          } else {
            res.status(HttpStatus.UNAUTHORIZED)
              .json({ message: 'passwords did not match' });
          }
        });
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .send('Error login');
    }
  },
};
