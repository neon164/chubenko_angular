const { news } = require('../models/index');
const { user } = require('../models/index');

const HttpStatus = require('http-status-codes');

module.exports = {
  async getNews(req, res) {
    try {
      const newses = await news.findAll({
        include: [
          {
            model: user,
            as: 'user',
            attributes: [
              'id',
              'firstName',
              'lastName',
              'img',
            ],
          },
        ],
        attributes: [
          'id',
          'name',
          'text',
          'authorId',
          'tags',
          'img',
        ],
        order: [['id', 'ASC']],
      });

      res.send(newses);
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .send('Error find news');
    }
  },
  async addNews(req, res) {
    try {
      const body = JSON.parse(req.body.text);
      const { name, text, tags, authorId } = body;

      let urlImg = null;

      if (req.file !== undefined) {
        urlImg = 'api/images/' + req.file.filename;
      }

      const newNews = await news.create({
        name,
        text,
        tags,
        authorId,
        img: urlImg,
      });

      res.send(newNews);
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .send('Error create news');
    }
  },
  async getByAuthor(req, res) {
    try {
      const newses = await news.findAll({ where: { authorId: req.params.id } });

      res.send(newses);
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .send('Error find news');
    }
  },
};
