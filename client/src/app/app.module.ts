import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewsComponentComponent } from './news-component/news-component.component';
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { SearchPipe } from "./search.pipe";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './login/login.component';
import { UserpageComponent } from './userpage/userpage.component';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider } from "angular-6-social-login";
import { RegistrationComponent } from './registration/registration.component';
import { AddnewsComponent } from './addnews/addnews.component';
import { NgxPaginationModule } from "ngx-pagination";

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("1077738248986-p3kg3oogmhqn8k6erkvhu0nb70cbcnp0.apps.googleusercontent.com")
      },
    ]
  );
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    NewsComponentComponent,
    SearchPipe,
    LoginComponent,
    UserpageComponent,
    RegistrationComponent,
    AddnewsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    SocialLoginModule,
    NgxPaginationModule
  ],
  providers: [
    { provide: AuthServiceConfig, useFactory: getAuthServiceConfigs }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
