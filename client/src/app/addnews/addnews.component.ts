import { Component } from '@angular/core';
import { AddnewsService, News } from "./addnews.service";
import { Router } from "@angular/router";
import { JwtHelperService } from "@auth0/angular-jwt";

@Component({
  selector: 'app-addnews',
  templateUrl: './addnews.component.html',
  styleUrls: ['./addnews.component.css'],
  providers: [AddnewsService],
})
export class AddnewsComponent {
  newsData: News = new News();
  userID: string;

  constructor(private addnewsService: AddnewsService, private router: Router) { }

  onSelected(event) {
    this.newsData.img = event.target.files[0];
  }
  sendNews(news: News) {
    this.addnewsService.postNews(news)
      .subscribe(() => {
        const token = localStorage.getItem('token');
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(token);
        this.userID = decodedToken.id;
        this.addnewsService.clearFormData();
        this.router.navigate(['userpage/' + this.userID]);
      },
    error => {
        this.addnewsService.clearFormData();
      });
  }
}
