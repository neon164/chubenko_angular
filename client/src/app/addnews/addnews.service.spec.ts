import { TestBed } from '@angular/core/testing';

import { AddnewsService } from './addnews.service';

describe('AddnewsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddnewsService = TestBed.get(AddnewsService);
    expect(service).toBeTruthy();
  });
});
