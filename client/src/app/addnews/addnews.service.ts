import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()

export class AddnewsService {
  userID: number;
  formData = new FormData();

  constructor(private http: HttpClient) { }

  postNews(news: News) {
    const token = localStorage.getItem('token');
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    this.userID = Number(decodedToken.id);

    const body = {name: news.name, text: news.text, tags: news.tags, authorId: this.userID};
    const config = {
      headers: {Authorization: 'Bearer ' + token},
    };

    this.formData.append('text', JSON.stringify(body));
    this.formData.append('img', news.img );
    return this.http.post('http://192.168.0.103:3000/api/news', this.formData, config);
  }
  clearFormData() {
    this.formData = new FormData();
  }
}

export class News {
  name: string;
  text: string;
  tags: string;
  img: File;
  authorId: number;
}
