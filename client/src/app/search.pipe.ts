import {Pipe, PipeTransform} from "@angular/core";
import { NewsComponentComponent } from "./news-component/news-component.component";

@Pipe({
  name: 'search'
})

export class SearchPipe implements PipeTransform{
  transform(newNews, value, selectedValue) {
    if (newNews !== undefined) {
      let filter = newNews.filter(newNews => {
        if (selectedValue === "All") {
          return newNews.name.includes(value) ||
            newNews.text.includes(value) ||
            newNews.user.firstName.includes(value) ||
            newNews.user.lastName.includes(value) ||
            (newNews.user.firstName + ' ' + newNews.user.lastName).includes(value) ||
            newNews.tags.includes(value)
        } else if (selectedValue === "Author") {
          return newNews.user.firstName.includes(value) ||
            newNews.user.lastName.includes(value) ||
            (newNews.user.firstName + ' ' + newNews.user.lastName).includes(value)
        } else if (selectedValue === "Tags") {
          return newNews.tags.includes(value)
        }
      });
      NewsComponentComponent.collectionSize = filter.length;
      return filter;
    }
  }
}
