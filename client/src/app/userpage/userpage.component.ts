import {Component, OnInit} from '@angular/core';
import {UpdateUser, UserpageService} from './userpage.service';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-userpage',
  templateUrl: './userpage.component.html',
  styleUrls: ['./userpage.component.css'],
  providers: [UserpageService]
})
export class UserpageComponent implements OnInit {

  private userData: object;
  private newses: object;
  p: number = 1;
  collection: any = this.userData;
  condition: string = '';
  private id: any;
  private routeSubscription: Subscription;
  updateUserData: UpdateUser = new UpdateUser;
  formData = new FormData();
  img: any;

  constructor(private userpageService: UserpageService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getUser();
      this.getNews();
      this.checkCondition();
      this.clearFormData();
    });
  }

  getUser() {
    this.userpageService.getUser().subscribe(data => {
      this.userData = data;
    });
  }
  getNews() {
    this.userpageService.getNewsByAuthor()
      .subscribe(data => {
      this.newses = data;
    });
  }
  addNews() {
    this.router.navigate(['addnews/']);
  }
  checkCondition() {
    const token = localStorage.getItem('token');
    if (token) {
      const helper = new JwtHelperService();
      const decodedToken = helper.decodeToken(token);
      if (decodedToken.id === Number(this.id)) {
        this.condition = 'true';
      } else {
        this.condition = '';
      }
    }
  }
  onSelected(event) {
    this.img = event.target.files[0];
  }
  clearFormData() {
    this.formData = new FormData();
  }
  updateUser(body: UpdateUser) {
    this.formData.append('img', this.img);
    this.formData.append('text', JSON.stringify(body));
    this.userpageService.updateUser(this.formData)
      .subscribe(data => {
        this.ngOnInit();
      });
  }
}
