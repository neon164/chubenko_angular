import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserpageService {

  private id: number;
  private routeSubscription: Subscription;

  constructor(private http: HttpClient, private route: ActivatedRoute) {
    this.routeSubscription = route.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  getUser() {
    return this.http.get('/api/userpage/' + this.id);
  }
  getNewsByAuthor() {
    return this.http.get('/api/news/' + this.id);
  }
  updateUser(body) {
    const token = localStorage.getItem('token');
    const config = {
      headers: {Authorization: 'Bearer ' + token},
    };

    return this.http.put('api/userpage/' + this.id, body, config);
  }
}
export class UpdateUser {
  firstName: string;
  lastName: string;
  img: string;
}
