import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from "@auth0/angular-jwt";
import { Router } from "@angular/router";
import { UserpageComponent } from "./userpage/userpage.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserpageComponent]
})
export class AppComponent implements OnInit{
  title = 'client';
  userID: string;
  condition: string;
  private id: number;

  constructor(private router: Router) {
  }
  ngOnInit () {
    this.checkCondition()
  }
  checkCondition() {
    this.condition = localStorage.getItem('token')
  }
  logOut() {
    localStorage.removeItem('token');
    this.router.navigate(['/']);
    this.checkCondition();
  }

  getUserPage() {
    const token = localStorage.getItem('token');
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    this.userID = decodedToken.id;
    this.router.navigate(['userpage/'+this.userID])
  }
}
