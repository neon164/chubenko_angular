import { Component } from '@angular/core';
import { LoginService, User } from './login.service';
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { AppComponent } from "../app.component";
import { AuthService,GoogleLoginProvider } from "angular-6-social-login";
import { AddUserHttpService } from "../registration/registration.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService,AddUserHttpService]
})
export class LoginComponent {

  userData: User = new User();
  receivedUser = {};
  incorrectData: string = '';
  private name: string;
  private email: string;

  constructor(private http: HttpClient,
              private httpService: AddUserHttpService,
              private LoginService: LoginService,
              private socialAuthService: AuthService,
              private router: Router,
              private appComponent: AppComponent) {
  }

  sendUserData(user: User) {
    this.LoginService.postData(user)
      .subscribe(
        (data: any) => {
          this.receivedUser = data.user;
          let token: string = data.token;
          localStorage.setItem('token', token);
          this.router.navigate(['userpage/' + data.user.id]);
          this.appComponent.checkCondition();
        },
        error => this.incorrectData = 'true'
      );
  }
  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        this.name = userData.name;
        this.email = userData.email;
        this.httpService.postNewSocialUser(userData)
          .subscribe((data: any) => {
            localStorage.setItem('token', data.token);
            const userID = data.id;
            this.router.navigate(['userpage/' + userID]);
            this.appComponent.checkCondition();
          });
      }
    );
  }
  sendToRegistartion(){
    this.router.navigate(['registration/']);
  }
}
