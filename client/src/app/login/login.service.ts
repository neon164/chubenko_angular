import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LoginService {

  constructor(private http: HttpClient) { }

  postData(user: User) {

    const body = {email: user.email, password: user.password};
    return this.http.post('http://192.168.0.103:3000/api/login', body);
  }
  postNewSocialUser (user: socialUser) {
    const body = {email: user.email, firstName: user.name };
    return this.http.post('http://192.168.0.103:3000/api/userpage/social', body);
  }
}
export class User {
  email: string;
  password: string;
}
export class socialUser {
  name: string;
  email: string;
  id: string;
}
