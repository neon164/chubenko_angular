import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NewsComponentComponent } from "./news-component/news-component.component";
import { UserpageComponent } from "./userpage/userpage.component";
import {RegistrationComponent} from "./registration/registration.component";
import { AddnewsComponent } from "./addnews/addnews.component";

const routes: Routes = [
  { path: '', component: NewsComponentComponent },
  { path: 'login', component: LoginComponent },
  { path: 'userpage/:id', component: UserpageComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'addnews', component : AddnewsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

