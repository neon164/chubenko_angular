import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class NewsServiceService {

  constructor(private http: HttpClient) { }

  getNews() {
    return this.http.get('/api/news');
  }
}
