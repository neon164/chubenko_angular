import { Component, OnInit } from '@angular/core';
import { NewsServiceService } from "./news-service.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-news-component',
  templateUrl: './news-component.component.html',
  styleUrls: ['./news-component.component.css']
})

export class NewsComponentComponent implements OnInit {
  private newNews: Object;
  private selectedValue = 'All';
  private searchString = '';
  private page = 1;
  private pageSize = 5;
  static collectionSize: Number;

  constructor(private newsService: NewsServiceService, private router: Router) {
  }

  ngOnInit() {
    this.getNews();
  }

  getNews() {
    this.newsService.getNews().subscribe(data => {
      this.newNews = data
    })
  }

  get callSize() {
    return NewsComponentComponent.collectionSize
  }
  showAuthorPage(data) {
    this.router.navigate(['userpage/'+data.id])
  }
}
