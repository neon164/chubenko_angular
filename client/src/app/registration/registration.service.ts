import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class AddUserHttpService {

  constructor(private http: HttpClient) {}

  postNewUser(user: User) {
    const body = {firstName: user.firstName, lastName: user.lastName, email: user.email, password: user.password, createdAt: user.createdAt, updatedAt: user.updatedAt};
    return this.http.post('http://192.168.0.103:3000/api/userpage', body, {responseType: 'json'});
  }

  postNewSocialUser(user: socialUser) {
    const body = {email: user.email, firstName: user.name };
    return this.http.post('http://192.168.0.103:3000/api/userpage/social', body);
  }

}
export class User {
  lastName: string;
  email: string;
  password: string;
  firstName: string;
  createdAt = new Date();
  updatedAt = new Date();
  id: string;
}
export class socialUser {
  name: string;
  email: string;
  id: string;
}
