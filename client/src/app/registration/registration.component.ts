import { Component } from '@angular/core';
import {AuthService, GoogleLoginProvider} from 'angular-6-social-login';
import { Router } from '@angular/router';
import { AddUserHttpService, User } from './registration.service';
import { AppComponent } from "../app.component";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [AddUserHttpService]
})
export class RegistrationComponent {
  userReg: User = new User();
  private receivedUser: {};
  private token: string;
  private name: string;
  private email: string;
  incorrectData: string = '';
  constructor(private httpService: AddUserHttpService,
              private socialAuthService: AuthService,
              private router: Router,
              private appComponent: AppComponent) { }

  submit(user: User) {
    this.httpService.postNewUser(user)
      .subscribe(
        (data: any) => {
          this.receivedUser = data;
          this.token = data.token;
          localStorage.setItem('token', this.token );
          this.router.navigate(['userpage/' + data.id]);
          this.appComponent.checkCondition();
          },
        error => {
          this.incorrectData = 'true'
        }
      );
  }
  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        this.name = userData.name;
        this.email = userData.email;
        this.httpService.postNewSocialUser(userData)
          .subscribe((data:any) => {
            localStorage.setItem('token', data.token);
            let userID = data.id;
            this.router.navigate(['userpage/' + userID]);
            this.appComponent.checkCondition();
          });
      }
    );
  }
}
